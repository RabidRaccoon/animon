# Animon Story (Unofficial)

This System for [Foundry Virtual Tabletop](https://foundryvtt.com/) provides the character sheets and game system for [Animon Story](https://www.animonstory.com/) by [Zak Borough](https://www.zakbarouh.com/).

This system provides character sheet support for Child and Animon characters.

This is an unofficial package and a copy of the [rulebook](https://zak-barouh.itch.io/animon-story) will be required to use this system.

Animon Story is copyright of Zak Barouh. This product FoundryVTT adaption has been produced independently based on the Animon [Creator’s League 3rd party agreement](https://www.animonstory.com/creatorsleague), and is unaffiliated with Zak Barouh.

## Features

- Support for Child and Animon Characters
- The Animon sheet allows for quick transformation between stages with states automatically updated
- Animon secondary stats are automatically calculated when primary stats are updated
- Score improvements from advancement are automatically applied when selected