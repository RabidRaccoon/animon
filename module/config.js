export const animon = {};

//Roll types
animon.rollType = {
    6: "animon.chat.statCheck.dSetback",
    5: "animon.chat.statCheck.setback",
    4: "animon.chat.statCheck.normal",
    3: "animon.chat.statCheck.boost",
    2: "animon.chat.statCheck.dSetback"
}

animon.stages = {
    fledgling: "animon.animon.stages.fledgling",
    basic: "animon.animon.stages.basic",
    super: "animon.animon.stages.super",
    ultra: "animon.animon.stages.ultra",
    giga: "animon.animon.stages.giga"
}
